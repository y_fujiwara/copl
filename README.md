# README #

本リポジトリはプログラミング言語の基礎概念勉強用のリポジトリです

### What is this repository for? ###

* 本リポジトリのソース・ファイルを以下のサイトの演習システムで実行します
  * http://www.fos.kuis.kyoto-u.ac.jp/~igarashi/CoPL/
* エディタ:vim 以下を利用しています
  * https://github.com/ymyzk/vim-copl
